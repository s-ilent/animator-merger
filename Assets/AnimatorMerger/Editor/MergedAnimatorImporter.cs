﻿#if VRC_SDK_VRCSDK3
using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEditor.Animations;
using VRC.SDK3.Avatars.Components;
using VRC.SDKBase;

#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace SilentTools.MergedAnimator
{

    [CanEditMultipleObjects]
    [ScriptedImporter(k_VersionNumber, MergedAnimatorImporter.kFileExtension)]
    public class MergedAnimatorImporter : ScriptedImporter
    {
        [Tooltip("The main animator to merge into.")]
        [SerializeField]
        AnimatorController m_MainAnimatorController;

        [Tooltip("The animators to merge into the main.")]
        [SerializeField]
        List<AnimatorController> m_SubAnimatorControllers = new List<AnimatorController>();

#if UNITY_2020_1_OR_NEWER
        const int k_VersionNumber = 202010;
#else
        const int k_VersionNumber = 201940;
#endif

        /// The file extension used for MergedAnimator assets without leading dot.
        public const string kFileExtension = "mergedanimator";

        public override void OnImportAsset(AssetImportContext ctx)
        {
            // Mark all input animators as dependency to the merged animator.
            // This should cause the merged animator to get re-generated when any input changes or when the build target changed.
            {
                var source = m_MainAnimatorController;
                var path = AssetDatabase.GetAssetPath(source);
#if UNITY_2020_1_OR_NEWER
                    ctx.DependsOnArtifact(path);
#else
                    ctx.DependsOnSourceAsset(path);
#endif
            }

            for (var n = 0; n < m_SubAnimatorControllers.Count; ++n)
            {
                var source = m_SubAnimatorControllers[n];
                if (source != null)
                {
                    var path = AssetDatabase.GetAssetPath(source);
#if UNITY_2020_1_OR_NEWER
                    ctx.DependsOnArtifact(path);
#else
                    ctx.DependsOnSourceAsset(path);
#endif
                }
            }
#if !UNITY_2020_1_OR_NEWER
            // This value is not really used in this importer,
            // but getting the build target here will add a dependency to the current active buildtarget.
            // Because DependsOnArtifact does not exist in 2019.4, adding this dependency on top of the DependsOnSourceAsset
            // will force a re-import when the target platform changes in case it would have impacted any animator this importer depends on.
            var buildTarget = ctx.selectedBuildTarget;
#endif

            // TODO: 
            // Check that any input controllers are NOT part of this asset
            
            // oh god I hope this isn't by reference
            AnimatorController mainController = new AnimatorController();
            AnimatorCloner.ResetControllerID();
            AnimatorCloner.MergeControllers(mainController, m_MainAnimatorController, ctx);

            for (var n = 0; n < m_SubAnimatorControllers.Count; ++n)
            {
                var source = m_SubAnimatorControllers[n];
                if (source != null)
                {
                    AnimatorCloner.MergeControllers(mainController, source, ctx);
                }
            }

            ctx.AddObjectToAsset("mainAnimator", mainController);
            ctx.SetMainObject(mainController);
        }

        [MenuItem("Assets/Create/Merged Animator", priority = 310)]
        static void CreateMergedAnimatorMenuItem()
        {
            var kMergedAnimatorAssetContent = "This file represents a MergedAnimator asset for Unity.\nYou need the 'MergedAnimator' package to properly import this file in Unity.";
            // https://forum.unity.com/threads/how-to-implement-create-new-asset.759662/
            string directoryPath = "Assets";
            foreach (Object obj in Selection.GetFiltered(typeof(Object), SelectionMode.Assets))
            {
                directoryPath = AssetDatabase.GetAssetPath(obj);
                if (!string.IsNullOrEmpty(directoryPath) && File.Exists(directoryPath))
                {
                    directoryPath = Path.GetDirectoryName(directoryPath);
                    break;
                }
            }
            directoryPath = directoryPath.Replace("\\", "/");
            if (directoryPath.Length > 0 && directoryPath[directoryPath.Length - 1] != '/')
                directoryPath += "/";
            if (string.IsNullOrEmpty(directoryPath))
                directoryPath = "Assets/";

            var fileName = string.Format("New MergedAnimator.{0}", kFileExtension);
            directoryPath = AssetDatabase.GenerateUniqueAssetPath(directoryPath + fileName);
            ProjectWindowUtil.CreateAssetWithContent(directoryPath, kMergedAnimatorAssetContent);
        }
    }
}

#endif