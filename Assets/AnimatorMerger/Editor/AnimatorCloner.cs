// Based on https://github.com/VRLabs/VRChat-Avatars-3.0/blob/master/Avatars%203.0%20Manager/Editor/AnimatorCloner.cs
// Copyright (c) 2021 VRLabs. Licensed under the MIT license. 

#if VRC_SDK_VRCSDK3
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;
using VRC.SDK3.Avatars.Components;
using VRC.SDKBase;

#if UNITY_2020_2_OR_NEWER
using UnityEditor.AssetImporters;
#else
using UnityEditor.Experimental.AssetImporters;
#endif

namespace SilentTools.MergedAnimator
{
    public static class AnimatorCloner
    {
        private static Dictionary<string, string> _parametersNewName;
        private static AssetImportContext _assetContext;
        private static int behaviourUniqueID;
        private static int childBehaviourUniqueID;
        private static int animatorStateUniqueID;
        private static int blendTreeUniqueID;
        private static int animatorTransitionUniqueID;
        private static int currentControllerID;

        public static void ResetControllerID() {
            currentControllerID = 0;
        }
        public static AnimatorController MergeControllers(AnimatorController mainController, AnimatorController controllerToMerge, 
            AssetImportContext assetContext,
            Dictionary<string, string> paramNameSwap = null)
        {
            if (mainController == null)
            {
                return null;
            }

            _parametersNewName = paramNameSwap ?? new Dictionary<string, string>();
            _assetContext = assetContext;
            behaviourUniqueID = 0;
            childBehaviourUniqueID = 0;
            animatorStateUniqueID = 0;
            blendTreeUniqueID = 0;
            animatorTransitionUniqueID = 0;


            if (controllerToMerge == null)
            {
                return mainController;
            }

            foreach (var p in controllerToMerge.parameters)
            {
                var newP = new AnimatorControllerParameter
                {
                    name = _parametersNewName.ContainsKey(p.name) ? _parametersNewName[p.name] : p.name,
                    type = p.type,
                    defaultBool = p.defaultBool,
                    defaultFloat = p.defaultFloat,
                    defaultInt = p.defaultInt
                };
                if (mainController.parameters.Count(x => x.name.Equals(newP.name)) == 0)
                {
                    mainController.AddParameter(newP);
                }
            }

            for (int i = 0; i < controllerToMerge.layers.Length; i++)
            {
                AnimatorControllerLayer newL = CloneLayer(controllerToMerge.layers[i], i == 0);
                newL.name = MakeLayerNameUnique(newL.name, mainController);
                newL.stateMachine.name = newL.name;
                mainController.AddLayer(newL);
            }
            currentControllerID++;
            return mainController;
        }

        private static string MakeLayerNameUnique(string name, AnimatorController controller)
        {
            string st = "";
            int c = 0;
            bool combinedNameDuplicate = controller.layers.Count(x => x.name.Equals(name)) > 0;
            while (combinedNameDuplicate)
            {
                c++;
                combinedNameDuplicate = controller.layers.Count(x => x.name.Equals(name + st + c)) > 0;
            }
            if (c != 0)
            {
                st += c;
            }

            return name + st;
        }

        private static AnimatorControllerLayer CloneLayer(AnimatorControllerLayer old, bool isFirstLayer = false)
        {
            var n = new AnimatorControllerLayer
            {
                avatarMask = old.avatarMask,
                blendingMode = old.blendingMode,
                defaultWeight = isFirstLayer ? 1f : old.defaultWeight,
                iKPass = old.iKPass,
                name = old.name,
                syncedLayerAffectsTiming = old.syncedLayerAffectsTiming,
                stateMachine = CloneStateMachine(old.stateMachine)
            };
            CloneTransitions(old.stateMachine, n.stateMachine);
            return n;
        }

        private static AnimatorStateMachine CloneStateMachine(AnimatorStateMachine old)
        {
            var n = new AnimatorStateMachine
            {
                anyStatePosition = old.anyStatePosition,
                entryPosition = old.entryPosition,
                exitPosition = old.exitPosition,
                hideFlags = old.hideFlags,
                name = old.name,
                parentStateMachinePosition = old.parentStateMachinePosition,
                stateMachines = old.stateMachines.Select(x => CloneChildStateMachine(x)).ToArray(),
                states = old.states.Select(x => CloneChildAnimatorState(x)).ToArray()
            };
            
            string name = "animatorState" + currentControllerID + "_" + animatorStateUniqueID.ToString() + n.name; animatorStateUniqueID++;
            _assetContext.AddObjectToAsset(name, n);
            n.defaultState = FindState(old.defaultState, old, n);
/*
            foreach (var oldb in old.behaviours)
            {
                var behaviour = n.AddStateMachineBehaviour(old.GetType());
                CloneBehaviourParameters(oldb, behaviour);
            }
*/
            StateMachineBehaviour[] newBehaviours = new StateMachineBehaviour[old.behaviours.Length];
            int i = 0;
            foreach (var oldb in old.behaviours)
            {
                //StateMachineBehaviour behaviour = (StateMachineBehaviour)old.GetType().GetConstructor(Type.EmptyTypes).Invoke(new object[]{});
                StateMachineBehaviour behaviour = (StateMachineBehaviour)ScriptableObject.CreateInstance(old.GetType());
                behaviour.hideFlags = old.hideFlags;
                // StateMachineBehaviours tend to not have names by default
                string bname = "behaviour" + currentControllerID + "_" + behaviourUniqueID.ToString() + oldb.name; behaviourUniqueID++;
                _assetContext.AddObjectToAsset(bname, behaviour);
                newBehaviours[i] = behaviour;
                i++;
                CloneBehaviourParameters(oldb, behaviour);
            }
            n.behaviours = newBehaviours;
            return n;
        }

        private static ChildAnimatorStateMachine CloneChildStateMachine(ChildAnimatorStateMachine old)
        {
            var n = new ChildAnimatorStateMachine
            {
                position = old.position,
                stateMachine = CloneStateMachine(old.stateMachine)
            };
            return n;
        }

        private static ChildAnimatorState CloneChildAnimatorState(ChildAnimatorState old)
        {
            var n = new ChildAnimatorState
            {
                position = old.position,
                state = CloneAnimatorState(old.state)
            };
/*
            foreach (var oldb in old.state.behaviours)
            {
                var behaviour = n.state.AddStateMachineBehaviour(oldb.GetType());
                CloneBehaviourParameters(oldb, behaviour);
            }
*/
            StateMachineBehaviour[] newBehaviours = new StateMachineBehaviour[old.state.behaviours.Length];
            int i = 0;
            foreach (var oldb in old.state.behaviours)
            {
                //StateMachineBehaviour behaviour = (StateMachineBehaviour)oldb.GetType().GetConstructor(Type.EmptyTypes).Invoke(new object[]{});
                StateMachineBehaviour behaviour = (StateMachineBehaviour)ScriptableObject.CreateInstance(oldb.GetType());
                behaviour.hideFlags = old.state.hideFlags;
                // StateMachineBehaviours tend to not have names by default
                string name = "childBehaviour" + currentControllerID + "_" + childBehaviourUniqueID.ToString() + oldb.name; childBehaviourUniqueID++;
                _assetContext.AddObjectToAsset(name, behaviour);
                newBehaviours[i] = behaviour;
                i++;
                CloneBehaviourParameters(oldb, behaviour);
            }
            n.state.behaviours = newBehaviours;
            return n;
        }

        private static AnimatorState CloneAnimatorState(AnimatorState old)
        {
            // Checks if the motion is a blend tree, to avoid accidental blend tree sharing between animator assets
            Motion motion = old.motion;
            if (motion is BlendTree oldTree)
            {
                var tree = CloneBlendTree(null, oldTree);
                motion = tree;
                // need to save the blend tree into the animator
                tree.hideFlags = HideFlags.HideInHierarchy;
                string bname = "blendTree" + currentControllerID + "_" + blendTreeUniqueID.ToString() + motion.name; blendTreeUniqueID++;
                _assetContext.AddObjectToAsset(bname, motion);
            }

            var n = new AnimatorState
            {
                cycleOffset = old.cycleOffset,
                cycleOffsetParameter = old.cycleOffsetParameter,
                cycleOffsetParameterActive = old.cycleOffsetParameterActive,
                hideFlags = old.hideFlags,
                iKOnFeet = old.iKOnFeet,
                mirror = old.mirror,
                mirrorParameter = old.mirrorParameter,
                mirrorParameterActive = old.mirrorParameterActive,
                motion = motion,
                name = old.name,
                speed = old.speed,
                speedParameter = old.speedParameter,
                speedParameterActive = old.speedParameterActive,
                tag = old.tag,
                timeParameter = old.timeParameter,
                timeParameterActive = old.timeParameterActive,
                writeDefaultValues = old.writeDefaultValues
            };
            string name = "animatorState" + currentControllerID + "_" + animatorStateUniqueID.ToString() + n.name; animatorStateUniqueID++;
            _assetContext.AddObjectToAsset(name, n);
            return n;
        }

        // Taken from here: https://gist.github.com/phosphoer/93ca8dcbf925fc006e4e9f6b799c13b0
        private static BlendTree CloneBlendTree(BlendTree newTree, BlendTree oldTree)
        {
            // Create a child tree in the destination parent, this seems to be the only way to correctly 
            // add a child tree as opposed to AddChild(motion)
            BlendTree pastedTree = newTree is null ? new BlendTree() : newTree.CreateBlendTreeChild(newTree.maxThreshold);
            pastedTree.name = oldTree.name;
            pastedTree.blendType = oldTree.blendType;
            pastedTree.blendParameter = oldTree.blendParameter;
            pastedTree.blendParameterY = oldTree.blendParameterY;
            pastedTree.minThreshold = oldTree.minThreshold;
            pastedTree.maxThreshold = oldTree.maxThreshold;
            pastedTree.useAutomaticThresholds = oldTree.useAutomaticThresholds;

            // Recursively duplicate the tree structure
            // Motions can be directly added as references while trees must be recursively to avoid accidental sharing
            foreach (var child in oldTree.children)
            {
                if (child.motion is BlendTree tree)
                {
                    var childTree = CloneBlendTree(pastedTree, tree);
                    // need to save the blend tree into the animator
                    childTree.hideFlags = HideFlags.HideInHierarchy;
                    string name = "blendTree" + currentControllerID + "_" + blendTreeUniqueID.ToString() + childTree.name; blendTreeUniqueID++;
                    _assetContext.AddObjectToAsset(name, childTree);
                }
                else
                {
                    var children = pastedTree.children;
                    ArrayUtility.Add(ref children, child);
                    pastedTree.children = children;
                }
            }

            return pastedTree;
        }

        private static void CloneBehaviourParameters(StateMachineBehaviour old, StateMachineBehaviour n)
        {
            if (old.GetType() != n.GetType())
            {
                throw new ArgumentException("2 state machine behaviours that should be of the same type are not.");
            }
            switch (n)
            {
                case VRCAnimatorLayerControl l:
                    {
                        var o = old as VRCAnimatorLayerControl;
                        l.ApplySettings = o.ApplySettings;
                        l.blendDuration = o.blendDuration;
                        l.debugString = o.debugString;
                        l.goalWeight = o.goalWeight;
                        l.layer = o.layer;
                        l.playable = o.playable;
                        break;
                    }
                case VRCAnimatorLocomotionControl l:
                    {
                        var o = old as VRCAnimatorLocomotionControl;
                        l.ApplySettings = o.ApplySettings;
                        l.debugString = o.debugString;
                        l.disableLocomotion = o.disableLocomotion;
                        break;
                    }
                case VRCAnimatorTemporaryPoseSpace l:
                    {
                        var o = old as VRCAnimatorTemporaryPoseSpace;
                        l.ApplySettings = o.ApplySettings;
                        l.debugString = o.debugString;
                        l.delayTime = o.delayTime;
                        l.enterPoseSpace = o.enterPoseSpace;
                        l.fixedDelay = o.fixedDelay;
                        break;
                    }
                case VRCAnimatorTrackingControl l:
                    {
                        var o = old as VRCAnimatorTrackingControl;
                        l.ApplySettings = o.ApplySettings;
                        l.debugString = o.debugString;
                        l.trackingEyes = o.trackingEyes;
                        l.trackingHead = o.trackingHead;
                        l.trackingHip = o.trackingHip;
                        l.trackingLeftFingers = o.trackingLeftFingers;
                        l.trackingLeftFoot = o.trackingLeftFoot;
                        l.trackingLeftHand = o.trackingLeftHand;
                        l.trackingMouth = o.trackingMouth;
                        l.trackingRightFingers = o.trackingRightFingers;
                        l.trackingRightFoot = o.trackingRightFoot;
                        l.trackingRightHand = o.trackingRightHand;
                        break;
                    }
                case VRCAvatarParameterDriver l:
                    {
                        var d = old as VRCAvatarParameterDriver;
                        l.ApplySettings = d.ApplySettings;
                        l.debugString = d.debugString;
                        l.localOnly = d.localOnly;
                        l.parameters = d.parameters.ConvertAll(p =>
                        {
                            string name = _parametersNewName.ContainsKey(p.name) ? _parametersNewName[p.name] : p.name;
                            return new VRC_AvatarParameterDriver.Parameter { name = name, value = p.value, chance = p.chance, valueMin = p.valueMin, valueMax = p.valueMax, type = p.type };
                        });
                        break;
                    }
                case VRCPlayableLayerControl l:
                    {
                        var o = old as VRCPlayableLayerControl;
                        l.ApplySettings = o.ApplySettings;
                        l.blendDuration = o.blendDuration;
                        l.debugString = o.debugString;
                        l.goalWeight = o.goalWeight;
                        l.layer = o.layer;
                        l.outputParamHash = o.outputParamHash;
                        break;
                    }
            }
        }

        private static AnimatorState FindState(AnimatorState original, AnimatorStateMachine old, AnimatorStateMachine n)
        {
            AnimatorState[] oldStates = GetStatesRecursive(old).ToArray();
            AnimatorState[] newStates = GetStatesRecursive(n).ToArray();
            for (int i = 0; i < oldStates.Length; i++)
                if (oldStates[i] == original)
                    return newStates[i];
            
            return null;
        }

        private static AnimatorStateMachine FindStateMachine(AnimatorStateTransition transition, AnimatorStateMachine sm)
        {
            AnimatorStateMachine[] childrenSm = sm.stateMachines.Select(x => x.stateMachine).ToArray();
            var dstSm = Array.Find(childrenSm, x => x.name == transition.destinationStateMachine.name);
            if (dstSm != null)
                return dstSm;

            foreach (var childSm in childrenSm)
            {
                dstSm = FindStateMachine(transition, childSm);
                if (dstSm != null)
                    return dstSm;
            }

            return null;
        }

        private static List<AnimatorState> GetStatesRecursive(AnimatorStateMachine sm)
        {
            List<AnimatorState> childrenStates = sm.states.Select(x => x.state).ToList();
            foreach (var child in sm.stateMachines.Select(x => x.stateMachine))
                childrenStates.AddRange(GetStatesRecursive(child));

            return childrenStates;
        }

        private static List<AnimatorStateMachine> GetStateMachinesRecursive(AnimatorStateMachine sm)
        {
            List<AnimatorStateMachine> childrenSm = sm.stateMachines.Select(x => x.stateMachine).ToList();

            List<AnimatorStateMachine> gcsm = new List<AnimatorStateMachine>();
            foreach (var child in childrenSm)
                gcsm.AddRange(GetStateMachinesRecursive(child));

            childrenSm.AddRange(gcsm);
            return childrenSm;
        }

        private static AnimatorState FindMatchingState(List<AnimatorState> old, List<AnimatorState> n, AnimatorTransitionBase transition)
        {
            for (int i = 0; i < old.Count; i++)
                if (transition.destinationState == old[i])
                    return n[i];

            return null;
        }
        
        private static AnimatorStateMachine FindMatchingStateMachine(List<AnimatorStateMachine> old, List<AnimatorStateMachine> n, AnimatorTransitionBase transition)
        {
            for (int i = 0; i < old.Count; i++)
                if (transition.destinationStateMachine == old[i])
                    return n[i];

            return null;
        }

        private static void CloneTransitions(AnimatorStateMachine old, AnimatorStateMachine n)
        {
            List<AnimatorState> oldStates = GetStatesRecursive(old);
            List<AnimatorState> newStates = GetStatesRecursive(n);
            List<AnimatorStateMachine> oldStateMachines = GetStateMachinesRecursive(old);
            List<AnimatorStateMachine> newStateMachines = GetStateMachinesRecursive(n);
            int j = 0;
            // Generate state transitions
            for (int i = 0; i < oldStates.Count; i++)
            {
                AnimatorStateTransition[] newTransitions = new AnimatorStateTransition[oldStates[i].transitions.Length];
                j = 0;
                foreach (var transition in oldStates[i].transitions)
                {
                    AnimatorStateTransition newTransition = new AnimatorStateTransition();
                    _assetContext.AddObjectToAsset("transition" + currentControllerID + "_" + animatorTransitionUniqueID.ToString(), newTransition);
                    animatorTransitionUniqueID++;
                    if (transition.isExit)
                    {
                        newTransition.isExit = true;
                    }
                    else if (transition.destinationState != null)
                    {
                        var dstState = FindMatchingState(oldStates, newStates, transition);
                        if (dstState != null)
                            newTransition.destinationState = dstState;
                    }
                    else if (transition.destinationStateMachine != null)
                    {
                        var dstState = FindMatchingStateMachine(oldStateMachines, newStateMachines, transition);
                        if (dstState != null)
                            newTransition.destinationStateMachine = dstState;
                    }

                    if (newTransition != null)
                        ApplyTransitionSettings(transition, newTransition);
                    newTransitions[j] = newTransition;
                    j++;
                }
                newStates[i].transitions = newTransitions;
            }

            // Generate AnyState transitions
            AnimatorStateTransition[] newAnystateTransitions = new AnimatorStateTransition[old.anyStateTransitions.Length];
            j = 0;
            foreach (var transition in old.anyStateTransitions)
            {
                AnimatorStateTransition newTransition = new AnimatorStateTransition();
                _assetContext.AddObjectToAsset("anytransition" + currentControllerID + "_" + animatorTransitionUniqueID.ToString(), newTransition);
                animatorTransitionUniqueID++;
                if (transition.destinationState != null)
                {
                    var dstState = FindMatchingState(oldStates, newStates, transition);
                    if (dstState != null)
                        newTransition.destinationState = dstState;
                }
                else if (transition.destinationStateMachine != null)
                {
                    var dstState = FindMatchingStateMachine(oldStateMachines, newStateMachines, transition);
                    if (dstState != null)
                        newTransition.destinationStateMachine = dstState;
                }
                
                if (newTransition != null)
                    ApplyTransitionSettings(transition, newTransition);
                newAnystateTransitions[j] = newTransition;
                j++;
            }
            n.anyStateTransitions = newAnystateTransitions;

            // Generate EntryState transitions
            AnimatorTransition[] newEntryTransitions = new AnimatorTransition[old.entryTransitions.Length];
            j = 0;
            foreach (var transition in old.entryTransitions)
            {
                AnimatorTransition newTransition = new AnimatorTransition();
                _assetContext.AddObjectToAsset("exittransition" + currentControllerID + "_" + animatorTransitionUniqueID.ToString(), newTransition);
                animatorTransitionUniqueID++;
                if (transition.destinationState != null)
                {
                    var dstState = FindMatchingState(oldStates, newStates, transition);
                    if (dstState != null)
                        newTransition.destinationState = dstState;
                }
                else if (transition.destinationStateMachine != null)
                {
                    var dstState = FindMatchingStateMachine(oldStateMachines, newStateMachines, transition);
                    if (dstState != null)
                        newTransition.destinationStateMachine = dstState;
                }
                
                if (newTransition != null)
                    ApplyTransitionSettings(transition, newTransition);
                j++;
            }
            n.entryTransitions = newEntryTransitions;
        }

        private static void ApplyTransitionSettings(AnimatorStateTransition transition, AnimatorStateTransition newTransition)
        {
            newTransition.canTransitionToSelf = transition.canTransitionToSelf;
            newTransition.duration = transition.duration;
            newTransition.exitTime = transition.exitTime;
            newTransition.hasExitTime = transition.hasExitTime;
            newTransition.hasFixedDuration = transition.hasFixedDuration;
            newTransition.hideFlags = transition.hideFlags;
            newTransition.isExit = transition.isExit;
            newTransition.mute = transition.mute;
            newTransition.name = transition.name;
            newTransition.offset = transition.offset;
            newTransition.interruptionSource = transition.interruptionSource;
            newTransition.orderedInterruption = transition.orderedInterruption;
            newTransition.solo = transition.solo;
            foreach (var condition in transition.conditions)
            {
                string conditionName = _parametersNewName.ContainsKey(condition.parameter) ? _parametersNewName[condition.parameter] : condition.parameter;
                newTransition.AddCondition(condition.mode, condition.threshold, conditionName);
            }
        }

        private static void ApplyTransitionSettings(AnimatorTransition transition, AnimatorTransition newTransition)
        {
            newTransition.hideFlags = transition.hideFlags;
            newTransition.isExit = transition.isExit;
            newTransition.mute = transition.mute;
            newTransition.name = transition.name;
            newTransition.solo = transition.solo;
            foreach (var condition in transition.conditions)
            {
                newTransition.AddCondition(condition.mode, condition.threshold, condition.parameter);
            }
        }
    }
}

#endif